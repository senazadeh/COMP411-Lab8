#include <stdio.h>

int NchooseK (int n, int k) {
    if (k == 0) { return 1; } 
    if (n == k) { return 1; } 
    else { return (NchooseK (n-1, k-1) + NchooseK (n-1, k)); }
}

int main()
{
    int n, k;
    while (1) {
        scanf("%d", &n);
        if (n == 0) {
            break;
        }
	scanf("%d", &k);
        printf("%d\n" ,NchooseK(n,k));
    }
}
