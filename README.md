# COMP411-Lab8

Lab 8: Procedure Calls and Recursion in Assembly
Due 11:55pm on Wednesday, October 30, 2019.

Exercise 1: Computing NchooseK recursively
C version
In this exercise, you are to re-write the recursive implementation of NchooseK() you wrote for Lab 3, with a minor change to the input format: the arguments n and k will be on separate lines in the input (because MARS can only handle one number per input line). The definition of the function is repeated here for convenience:

        NchooseK(n, 0) = 1
        NchooseK(n, n) = 1
        NchooseK(n, k) = NchooseK(n-1, k-1) + NchooseK(n-1, k)
All other assumptions from Lab 3 still hold. The implementation of the NchooseK function must be recursive. A non-recursive implementation of the function will receive zero credit. You can assume that the function will be called only with an argument small enough so that the result does not overflow, i.e., fits within a 32-bit integer.

First implement this function in C (or copy your implementation from Lab 3 if it was correct, and modify the part that reads the input). Note the following:

A starter file is provided: ex1.c.
The function should have the following name and type: int NchooseK(int n, int k)
The main() procedure repeatedly reads two lines of input, calls NchooseK() to compute NchooseK, and finally uses printf("%d\n",...) to print the NchooseK value.
These steps are repeated until a 0 value is read by main as the first argument, and then the program terminates.
Name the file with your C code ex1.c. Test it using your own inputs, and also the sample inputs provided.

Assembly version
Once your C code is working correctly, write the equivalent MIPS assembly program, using the provided starter file (ex1.asm). Since you are implementing a procedure that is recursive, you will need to appropriately manage the stack. That is the main challenge of this exercise.

Templates: Assembly coding templates have been provided for the main procedure (main_template4b.asm) and for other procedures (proc_template4b.asm). To simplify the assembly coding, these templates assume the following: (i) no procedure call needs more than 4 arguments; (ii) any local variables are placed in registers and not created on the stack; (iii) no "temporary" registers need to be protected from a subsequent call to another procedure. These templates correspond to Example 4b on Slide 37 of Lecture 9 (Procedure and Stacks).

Assembly coding templates corresponding to Example 5a on Slide 38 of the lecture are also available (main_template5a.asm and proc_template5a.asm)). The only difference here is that these templates also allow you the ability to save "temporaries" on the stack when they need to be protected from subsequent procedure calls.

Our recommendation is to construct your assembly code in such a manner that the simpler "4b" templates can be used. In particular, if, say, proc1 calls proc2, and proc1 has some important values in temporary registers that it must protect from proc2, then proc1 should copy these important values to "saved" registers (e.g., $s0, $s1, etc.), because it is guaranteed that these values will be the same upon return from proc2. However, if you find the style of the "5a" templates more to your liking, you may cerainly use those.

Factorial Example: As an example of how to use these templates to write a recursive procedure, assembly code is provided for calculating factorial in a recursive fashion, using the "4b" templates. Please carefully study the code for the main procedure (fact_main4b.asm), as well as the recursive factorial procedure (fact_proc4b.asm). The two procedures have been concatenated together into a single assembly file (fact4b.asm); open this file in MARS and run it step-by-step to follow the execution of the recursive procedure. Use these files as guidance to develop your code for this exercise.

Method: We suggest you initially develop your code for each procedure in a separate file using the templates provided: main_template4b.asm for main, and proc_template4b.asm for the procedure NchooseK. That will force you to maintain a clean separation between the codes of the two procedures.

To run your code in MARS, first make sure the two assembly files are located in the same folder, and that there are no other .asm files in that folder. Open the two assembly files in MARS. Under Settings, select "Assemble all files in directory". Next, in the Edit window, make sure that the file with main is selected, and then assemble and run.

Submission: Before submission, you will need to combine your code into a single assembly file. Concatenate the two files (main first), and save it as a single file ex1.asm. Move it to a different sub-/folder so it is the only .asm file in that folder. Open it in MARS and assemble and run it to make sure it still runs properly.

Note:

Your final code should have the overall structure of this starter file: ex1.asm.
Important: Convert your C program line-by-line to its assembly equivalent. Doing it any other way will take you much longer.
You must follow all of the procedure calling conventions discussed in class. Specifically, arguments must be passed to NchooseK in registers $a0 and $a1, and results should be returned in register $v0.
You have a lot of leeway in choosing the registers used by your procedures but you must follow the caller/callee save-restore conventions for these registers. Even if your code works, you will lose points if you do not follow the calling conventions.
In particular, $ra, $fp, and any saved registers ($s0-$s7) must be saved/restored according to conventions exactly as discussed in class.
More discussion of procedure calling conventions and stack management, including more assembly coding templates, is available in the lecture slides.
Name the file with all of your assembly code ex1.asm. Test it using your own inputs, and also the sample inputs provided.

Test Inputs, Due Date and Submission Procedure
Sample inputs and corresponding outputs are provided on the comp411-2fa19.cs.unc.edu server under /home/students/montek/comp411/samples/lab8.

Your assignment (i.e., the files ex1.c and ex1.asm) must be submitted electronically by 11:55pm on Wednesday, October 30, 2019.

Once you are satisfied that your assembly program is running fine within MARS on your laptop, copy the file ex1.asm to the server under the appropriate folder in your home directory (e.g., comp411lab/lab8). If you have developed your C code on the server, the file ex1.c should already be in that directory, else copy it to that folder. Then, run the self-checking script:

% cd ~/comp411lab/lab8
% cp /home/students/montek/comp411/samples/lab8/* .
% selfchecklab8

How to submit: If the final version of either of your programs were edited on the server, first transfer your work back to your laptop. Next, log in to Sakai in a browser window, and look for the lab under "Assignments" in the left panel. Attach the requested files and submit.

In case of any problems, please contact the instructor or the TAs.

24 October 2019, Montek Singh, montek@cs.unc.edu
